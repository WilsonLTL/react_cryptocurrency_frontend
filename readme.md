# react_cryptocurrency_frontend
## A frontend for Cryptocurrency
![imag1](https://i.imgur.com/Ym2MQGl.png)

## Intro Video
https://www.youtube.com/watch?v=2HkaCibwUPk

## UI Design
![image2](https://i.imgur.com/mEYRurU.png)

## Web App (PWA)
![image3](https://i.imgur.com/3Wx4WHh.gif)

### Local File tree
```.
├── Dockerfile
├── README.md
├── public
│   ├── css
│   │   └── index.css
│   ├── index.html
│   ├── manifest.json
│   └── icon.png
├── src
│   ├── action
│   │   └── backend_api.js
│   ├── components
│   │   ├── Card
│   │   │   ├── Card.js
│   │   │   └── index.js
│   │   ├── CardBody
│   │   │   ├── CardBody.js
│   │   │   └── index.js
│   │   ├── Card
│   │   │   ├── Card.js
│   │   │   └── index.js
│   │   ├── CardHeader
│   │   │   ├── CardHeader.js
│   │   │   └── index.js
│   │   └── Main
│   │       ├── Main.js
│   │       └── index.js
│   ├── constants
│   │       └── data.js
│   ├── reducers
│   │       ├── backend_api.js
│   │       └── index.js
│   └── index.js
├── package-lock.json
├── package.json
└── .gitlab-ci.yml
```

## 0. Prepare in your main server
```text
1. sudo apt-get update -qy
2. sudo apt install npm -qy
3. sudo apt install docker.io -qy
4. sudo apt install docker-compose -qy
```

## 1. Setup the main node server - Ubuntu
```text
1. npm i
2. docker login registry.gitlab.com -u wilsonlo1997@gmail.com -p e63LVHtsjTz5CzcSj6Yi
3. ocker pull registry.gitlab.com/wilsonltl/react_cryptocurrency_frontend
4. docker run -d -p 80:3000 registry.gitlab.com/wilsonltl/react_cryptocurrency_frontend
PS: Or you can "npm start"
```

## Q&A
```text
Q1: Frondend and backend framework?
A1: React with Redux, express
Q2: Responsive design?
A2: Sure
Q3: Scalability?
A3: Both frontend and backend are scalable with nginx as load balance too
Q4: consideration of charging?
A4: To solve this problem, only the node server have permission to update the redis data per min
Q5: Dockerizing?
A5: Both frondend, backend, db, load balance are base on docker image
```
