import Immutable from 'immutable';

export const Data = Immutable.fromJS({
    PostsData : [
        {
            "timestamp": "",
            "title": "",
            "price": "",
            "volume": "",
            "changeup": "",
            "changedown": "",
            "image": ""
        }
    ]
});