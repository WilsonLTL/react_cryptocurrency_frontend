import React from 'react';
import Card from '../Card';
import Grid from '@material-ui/core/Grid';
import { createStore } from 'redux'
import { update } from '../../actions/backend_api'
import backend_api from '../../reducers/backend_api'
import './scss/style.scss'


class Main extends React.Component {
    constructor() {
        super();

        this.state = {
            posts: {} };
    }
    componentWillMount() {
        let store = createStore(backend_api);
        let data = update();
        setInterval(() => store.dispatch({ type: 'UPDATE', payload: { data: data} }), 1000);
        store.subscribe(() => this.setState(store.getState()));
    }

    render() {
        return(
            <div>
                <header className="app-header"></header>
                <section className="app-title">
                    <div className="app-title-content">
                        <h1>Latest News</h1>
                    </div>
                </section>
                <Grid container justify="center" className="app-card-list" id="app-card-list">
                    {
                        Object.keys(this.state.posts).map(key => <Card key={key} index={key} details={this.state.posts[key]}/>)
                    }
                </Grid>
            </div>
        )
    }}

export default Main;