import React from 'react';
import CardHeader from '../CardHeader';
import CardBody from '../CardBody';

const Card = props => (
    <article className="card">
        <CardHeader image={props.details.image} category={"Coin"}/>
        <CardBody title={props.details.title} timestamp={props.details.timestamp} price={props.details.price} volume={props.details.volume} changeup={props.details.changeup} changedown={props.details.changedown}/>
    </article>
);

export default Card;