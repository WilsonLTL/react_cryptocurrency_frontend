import React from 'react';

const CardBody = (props) => {
    let date = new Date(props.timestamp*1000);
    return (
        <div className="card-body">
            <p className="date">{date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()}</p>
            <h2>{props.title}</h2>
            <div className="div_price">
                Price <span className="price">{props.price}</span>
            </div>
            <div className="div_volume">
                Volume <span className="volume">{props.volume}</span>
            </div>
            <div className="div_change">
                Change <span className="change_up">{props.changeup}</span><span className="change_down">{props.changedown}</span>
            </div>
        </div>
    );
};

export default CardBody;