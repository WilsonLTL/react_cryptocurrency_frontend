import React from 'react';

const CardHeader = (props)=> {
    const { image, category } = props;
    let style = {
        backgroundImage: 'url(' + image + ')' };

    return (
        <header style={style} className="card-header">
            <h4 className="card-header--title">{category}</h4>
        </header>
    )
};

export default CardHeader;