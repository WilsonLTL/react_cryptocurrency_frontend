const axios = require('axios');

export const update = () => {
    let PostsData = [
        {
            "timestamp": "1559055423",
            "title": "BTC to USD",
            "price": "8693.29701378",
            "volume": "87511.15188701",
            "changeup": "40.53338733",
            "changedown": "",
            "image": "https://files.chepicap.com/news/2018/11/29/v2_large_54f5dec421775fa21775c6ea224b1a3d156ae01f.jpg"
        },
        {
            "timestamp": "1559055423",
            "title": "ETH to USD",
            "price": "269.28815111",
            "volume": "1060615.64151309",
            "changeup": "1.91882149",
            "changedown": "",
            "image": "https://hacked.com/wp-content/uploads/2019/01/Ethereum-2.jpg"
        },
        {
            "timestamp": "1559055423",
            "title": "LTC to USD",
            "price": "114.39188932",
            "volume": "2543223.52305263",
            "changeup": "1.35670055",
            "changedown": "",
            "image": "https://ethereumworldnews.com/wp-content/uploads/2018/05/Litecoin-11-678x381.png"
        },
        {
            "timestamp": "1559055423",
            "title": "XMR to USD",
            "price": "96.41828086",
            "volume": "96115.20500682",
            "changeup": "0.40265965",
            "changedown": "",
            "image": "https://news.bitcoin.com/wp-content/uploads/2018/07/XMR-1520x1024.jpg"
        },
        {
            "timestamp": "1559055423",
            "title": "XRP to USD",
            "price": "0.42533837",
            "volume": "501488975.20468998",
            "changeup": "0.00112100",
            "changedown": "",
            "image": "https://cryptocoinspy.com/wp-content/uploads/2018/03/ripple-coin-xrp-cryptocurrency..jpg"
        },
        {
            "timestamp": "1559055423",
            "title": "DOGE to USD",
            "price": "0.00309787",
            "volume": "268886018.56458998",
            "changeup": "0.00001000",
            "changedown": "",
            "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJxS-u0Hs_P9ADj-ZeOtRIa7TffVGhPt7vDKt0DL3-rKXZPUZQ"
        },
        {
            "timestamp": "1559055423",
            "title": "DASH to USD",
            "price": "165.95220410",
            "volume": "20261.17684961",
            "changeup": "2.24053939",
            "changedown": "",
            "image": "https://img3.s3wfg.com/web/img/images_uploaded/e/a/dash_criptomoneda_620x350.jpg"
        },
        {
            "timestamp": "1559055423",
            "title": "MAID to USD",
            "price": "0.26397134",
            "volume": "0",
            "changeup": "0.00127343",
            "changedown": "",
            "image": "https://previews.123rf.com/images/artefacti/artefacti1707/artefacti170700109/81837747-copper-maidsafecoin-coin-isolated-on-white-background-3d-rendering.jpg"
        },
        {
            "timestamp": "1559055423",
            "title": "LSK to USD",
            "price": "2.08971445",
            "volume": "44833.07204926",
            "changeup": "0.02232477",
            "changedown": "",
            "image": "https://pbs.twimg.com/media/DkvAUlTWwAAr60J.jpg"
        },
    ];
    let api_url = "http://18.138.253.128:8080";
    let api_list = ["btc_usd","eth_usd","ltc_usd","xmr_usd","xrp_usd","doge_usd","dash_usd","maid_usd","lsk_usd"];
    api_list.forEach((item,index)=>{
        axios.post(api_url+"/react_cryptocurrency_backend/"+item).then((res)=>{
            let ticket = res.data["ticker"];
            PostsData[index]["timestamp"] = res.data["timestamp"];
            PostsData[index]["title"] = ticket["base"] + " to "+ ticket["target"];
            PostsData[index]["price"] = ticket["price"];
            PostsData[index]["changeup"] = "";
            PostsData[index]["changedown"] = "";
            ticket["volume"] === "" ? PostsData[index]["volume"] = 0 : PostsData[index]["volume"] = ticket["volume"];
            ticket["change"] > 0 ? PostsData[index]["changeup"] = ticket["change"] : PostsData[index]["changedown"] = ticket["change"];
        }).catch((err)=>{
            console.log("Warning: "+err.toString())
        });
    });
    console.log(PostsData);
    return PostsData
};